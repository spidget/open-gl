#include "gl.h"
#include "jpeg.h"
#include "transforms.h"

static char *read(char *path)
{
        FILE *f;
        int len;
        char *buf;

        f = fopen(path, "rb");
        if (!f) return NULL;

        fseek(f, 0, SEEK_END);

        len = ftell(f);

        buf = (char*) malloc(len + 1);
        if (!buf) return NULL;

        fseek(f, 0, SEEK_SET);
        fread(buf, len, 1, f);
        fclose(f);

        buf[len] = 0;

        return buf;
}

static void gl_fail(GLuint object,
                    PFNGLGETSHADERIVPROC glGet__iv,
                    PFNGLGETSHADERINFOLOGPROC glGet__InfoLog,
                    char *thing)
{
        GLint len;
        char *log;

        glGet__iv(object, GL_INFO_LOG_LENGTH, &len);
        log = malloc(len);
        glGet__InfoLog(object, len, NULL, log);
        fprintf(stderr, "%s: %s", thing, log);
        free(log);
        exit(1);
}

void setup_gl(struct resolution *res)
{
        GLenum err;

        glEnable(GL_DEPTH_TEST);
        err = glewInit();

        if (GLEW_OK != err) {
                fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
                exit(1);
        }

        get_resolution(res);
        glViewport(0, 0, res->width, res->height);
}

static GLuint create_shader(char *path, GLenum type)
{
        GLuint shdr;
        const GLchar *buf;
        int status;

        shdr = glCreateShader(type);

        buf = read(path);
        glShaderSource(shdr, 1, &buf, 0);
        free((char*) buf);

        glCompileShader(shdr);
        glGetShaderiv(shdr, GL_COMPILE_STATUS, &status);
        if (!status)
                gl_fail(shdr, glGetShaderiv, glGetShaderInfoLog, path);

        return shdr;
}

void load_tex(char *path)
{
        struct jpeg jpeg;

        read_jpeg(&jpeg, path);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
                     jpeg.width, jpeg.height, 0,
                     GL_RGB, GL_UNSIGNED_BYTE,
                     jpeg.b);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);

        free(jpeg.b);
}

void set_perspective(GLuint *program, struct resolution *res)
{
        float p[16];
        GLfloat u_proj;

        identity(p);
        perspective(p, 60, (float) res->width / res->height, 0.1, 200.0);

        u_proj = glGetUniformLocation(*program, "proj");

        glUniformMatrix4fv(u_proj, 1, GL_FALSE, p);
}

GLuint vao_setup(struct model *m)
{
        GLuint vao, vbo, ibo;

        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, m->vsize, m->verts, GL_STATIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                              5 * sizeof(GLfloat),
                              (void *) 0);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
                              5 * sizeof(GLfloat),
                              (void *) (3 * sizeof(GLfloat)));
        glEnableVertexAttribArray(1);

        glGenBuffers(1, &ibo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, m->isize, m->idx, GL_STATIC_DRAW);

        glBindVertexArray(0);
        glDeleteBuffers(1, &vbo);

        return vao;
}

GLuint compile(void)
{
        GLuint program, vshdr, fshdr;
        GLint status;

        program = glCreateProgram();

        vshdr = create_shader("glthing.vert", GL_VERTEX_SHADER);
        glAttachShader(program, vshdr);

        fshdr = create_shader("glthing.frag", GL_FRAGMENT_SHADER);
        glAttachShader(program, fshdr);

        glLinkProgram(program);
        glDetachShader(program, vshdr);
        glDeleteShader(vshdr);
        glDetachShader(program, fshdr);
        glDeleteShader(fshdr);

        glGetProgramiv(program, GL_LINK_STATUS, &status);
        if (!status)
                gl_fail(program,
                        glGetProgramiv,
                        glGetProgramInfoLog,
                        "program");

        glUseProgram(program);

        return program;
}
