#include <stdio.h>
#include <stdlib.h>

#include<jpeglib.h>        

#include "jpeg.h"

void read_jpeg(struct jpeg *jpeg, char* path)
{
        unsigned long data_size;
        unsigned char* rowptr[1];
        struct jpeg_decompress_struct info;
        struct jpeg_error_mgr err;

        FILE* file = fopen(path, "rb");
        if (!file)
                abort();

        info.err = jpeg_std_error(&err);
        jpeg_create_decompress(&info);

        jpeg_stdio_src(&info, file);        
        jpeg_read_header(&info, TRUE);

        jpeg_start_decompress(&info);

        jpeg->width = info.output_width;
        jpeg->height = info.output_height;

        data_size = jpeg->width * jpeg->height * 3;

        jpeg->b = (unsigned char*) malloc(data_size);
        int count = 0;
        while (info.output_scanline < info.output_height) {
                rowptr[0] = (unsigned char*) jpeg->b + 3
                        * info.output_width * info.output_scanline;
                jpeg_read_scanlines(&info, rowptr, 1);
                count++;
        }

        jpeg_finish_decompress(&info);
        jpeg_destroy_decompress(&info);

        fclose(file);
}
