#ifndef MATRIX_H_
#define MATRIX_H_

void identity(float *m);

void move(float *m, const float x, const float y, const float z);

void scale(float *m, const float amount);

void rotate_x(float *m, const float deg);
void rotate_y(float *m, const float deg);
void rotate_z(float *m, const float deg);

void perspective(float *m, float fov, float aspect, float n, float f);

struct vec {
        float x;
        float y;
        float z;
};
void cross_vec(struct vec *out, struct vec *a, struct vec *b);
void move_vec(struct vec *pos, struct vec *dir, float speed);

struct camera {
        struct vec pos;
        struct vec dir;
        struct vec up;
};
void look_at(float *m, struct camera *cam);

#endif
