#include <assert.h>
#include <math.h>
#include <stdio.h>

#include "transforms.h"

#ifndef M_PI
#    define M_PI 3.14159265358979323846
#endif

/* Multiply two 4x4 matrices. OpenGL uses column major matrices, represented as
 * an array of 16 floats:
 *
 * 00 04 08 12
 * 01 05 09 13
 * 02 06 10 14
 * 03 07 11 15
 *
 * The rows of one are multiplied by the columns of two.
 */
static void mul(float *one, const float *two)
{
        int x, y;
        float res[16];

        for (x = 0; x < 13; x+=4)
                for (y = 0; y < 4; y++)
                        res[x+y] = (one[y]    * two[x])
                                 + (one[y+4]  * two[x+1])
                                 + (one[y+8]  * two[x+2])
                                 + (one[y+12] * two[x+3]);

        for (x = 0; x < 16; x++)
                one[x] = res[x];
}

void identity(float *m)
{
        int i;

        for (i = 0; i < 16; i++)
                m[i] = (i % 5) ? 0.0 : 1.0;
}

void move(float *m, const float x, const float y, const float z)
{
        const float move[16] = {
                1.0, 0.0, 0.0, 0.0,
                0.0, 1.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0,
                  x,   y,   z, 1.0
        };
        mul(m, move);
}

void scale(float *m, const float s)
{
        const float scale[16] = {
                s, 0.0, 0.0, 0.0,
              0.0,   s, 0.0, 0.0,
              0.0, 0.0,   s, 0.0,
              0.0, 0.0, 0.0, 1.0
        };
        mul(m, scale);
}

static const float d2r = M_PI / 180;

void rotate_y(float *m, const float deg)
{
        const float rad = deg * d2r;
        const float c = cos(rad);
        const float s = sin(rad);
        const float rotate_xy[16] = {
                  c,   s, 0.0, 0.0,
                 -s,   c, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0,
                0.0, 0.0, 0.0, 1.0
        };
        mul(m, rotate_xy);
}

void rotate_z(float *m, const float deg)
{
        const float rad = deg * d2r;
        const float c = cos(rad);
        const float s = sin(rad);
        const float rotate_yz[16] = {
                  c, 0.0,   s, 0.0,
                0.0, 1.0, 0.0, 0.0,
                 -s, 0.0,   c, 0.0,
                0.0, 0.0, 1.0, 1.0
        };
        mul(m, rotate_yz);
}

void rotate_x(float *m, const float deg)
{
        const float rad = deg * d2r;
        const float c = cos(rad);
        const float s = sin(rad);
        const float rotate_x[16] = {
                1.0, 0.0, 0.0, 0.0,
                0.0,   c,   s, 0.0,
                0.0,  -s,   c, 0.0,
                0.0, 0.0, 0.0, 1.0
        };
        mul(m, rotate_x);
}

static void normalise(struct vec *v)
{
        const float mag = sqrt(v->x * v->x + v->y * v->y + v->z * v->z);

        v->x /= mag;
        v->y /= mag;
        v->z /= mag;
}

void cross_vec(struct vec *out, struct vec *a, struct vec *b)
{
        out->x = a->y * b->z - b->y * a->z;
        out->y = a->z * b->x - b->z * a->x;
        out->z = a->x * b->y - b->x * a->y;
}

void move_vec(struct vec *pos, struct vec *dir, float speed)
{
        pos->x += dir->x * speed;
        pos->y += dir->y * speed;
        pos->z += dir->z * speed;
}

void look_at(float *m, struct camera *cam)
{
        struct vec focus;
        struct vec forward;
        struct vec side;
        struct vec up2;

        focus = cam->pos;
        move_vec(&focus, &cam->dir, 1);

        forward.x = focus.x - cam->pos.x;
        forward.y = focus.y - cam->pos.y;
        forward.z = focus.z - cam->pos.z;

        normalise(&forward);
        cross_vec(&side, &forward, &cam->up);
        normalise(&side);
        cross_vec(&up2, &side, &forward);

        identity(m);

        m[0]  = side.x;
        m[4]  = side.y;
        m[8]  = side.z;

        m[1]  = up2.x;
        m[5]  = up2.y;
        m[9]  = up2.z;

        m[2]  = -forward.x;
        m[6]  = -forward.y;
        m[10] = -forward.z;

        move(m, -cam->pos.x, -cam->pos.y, -cam->pos.z);
}

static void frustum(float *m, float l, float r, float b, float t, float n, float f)
{
        float frustum[16] = {0};

        frustum[0]  = (2 * n) / (r - l);
        frustum[5]  = (2 * n) / (t - b);
        frustum[8]  = (r + l) / (r - l);
        frustum[9]  = (t + b) / (t - b);
        frustum[10] = -(f + n) / (f - n);
        frustum[11] = -1.0;
        frustum[14] = (-2 * f * n) / (f - n);

        mul(m, frustum);
}

void perspective(float *m, float fov, float aspect, float n, float f)
{
        float ymax, xmax;

        assert(n >= 0.1);
        assert(n < f);

        ymax = n * tanf(fov * M_PI / 360.0);
        xmax = ymax * aspect;
        frustum(m, -xmax, xmax, -ymax, ymax, n, f);
}
