#version 120

attribute vec3 pos;
attribute vec3 tex;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

varying vec3 out_colour;
varying vec2 tex_coord;

void main(void) {
    gl_Position = proj * view * model * vec4(pos.x, pos.y, pos.z, 1.0);
    tex_coord = vec2(tex.x, tex.y);
}
