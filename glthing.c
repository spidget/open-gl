#include <sys/time.h>
#include <time.h>

#include "game.h"
#include "gl.h"
#include "sdl.h"
#include "transforms.h"

#define TERRAIN 0
#define MODEL 1

static SDL_Window *window;
static struct resolution res;
static struct input input = {0};
static struct camera cam = {.pos = {0, 2, 0},
                            .dir = {1, 0, 1},
                            .up  = {0, 1, 0}};

long ticks(void)
{
        static struct timeval then = {0};
        struct timeval now;
        long ms;

        if (gettimeofday(&now, NULL) == -1)
                abort();

        if (!then.tv_sec)
                then = now;

        ms = now.tv_sec - then.tv_sec;
        ms *= 1000000;
        ms += now.tv_usec - then.tv_usec;

        then = now;

        return ms;
}

int main(int argc, char **argv)
{
        GLuint program, vao_model, vao_terrain;
        GLuint tex[2];
        struct model model, terrain;
        float m[16], v[16];
        GLfloat u_model, u_view;
        int deg;
        GLenum err;

        srand(time(NULL));
        window = init_sdl();
        setup_gl(&res);

        program = compile();
        set_perspective(&program, &res);

        create_terrain(&terrain);
        vao_terrain = vao_setup(&terrain);

        glGenTextures(sizeof tex / sizeof tex[0], tex);
        glBindTexture(GL_TEXTURE_2D, tex[TERRAIN]);
        load_tex("terrain.jpg");
        glBindTexture(GL_TEXTURE_2D, tex[MODEL]);
        load_tex("cube.jpg");

        create_model(&model);
        vao_model = vao_setup(&model);

        u_model = glGetUniformLocation(program, "model");
        u_view = glGetUniformLocation(program, "view");

        deg = 0;
        while (sdl_input(&input, &res)) {
                long ms = ticks();
                printf("%lu\n", ms);

                glClearColor(0.53, 0.80, 0.92, 1.0);
                glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

                move_cam(&input, &cam);

                if (input.keys.fullscreen) {
                        int fs;

                        fs = (SDL_GetWindowFlags(window)
                                        & SDL_WINDOW_FULLSCREEN_DESKTOP)
                                ? 0
                                : SDL_WINDOW_FULLSCREEN_DESKTOP;
                        SDL_SetWindowFullscreen(window, fs);
                }

                if (input.keys.resize) {
                        set_perspective(&program, &res);
                        glViewport(0, 0, res.width, res.height);
                        input.keys.resize = 0;
                }

                /* model */
                identity(m);
                move(m, -50, 0, -50);
                glUniformMatrix4fv(u_model, 1, GL_FALSE, m);

                /* view */
                look_at(v, &cam);
                glUniformMatrix4fv(u_view, 1, GL_FALSE, v);

                /* draw terrain */
                glBindVertexArray(vao_terrain);
                glBindTexture(GL_TEXTURE_2D, tex[TERRAIN]);
                glDrawElements(GL_TRIANGLES,
                               terrain.isize / sizeof(terrain.idx[0]),
                               GL_UNSIGNED_SHORT,
                               0);

                /* model */
                identity(m);
                move(m, -2, 1, 0);
                rotate_x(m, deg);
                glUniformMatrix4fv(u_model, 1, GL_FALSE, m);

                if(input.keys.spin)
                        deg = deg > 359 ? 0 : deg + 15;

                /* draw cube */
                glBindVertexArray(vao_model);
                glBindTexture(GL_TEXTURE_2D, tex[MODEL]);
                glDrawElements(GL_TRIANGLES,
                               model.isize / sizeof(model.idx[0]),
                               GL_UNSIGNED_SHORT,
                               0);

                while ((err = glGetError()) != GL_NO_ERROR) {
                        printf("glerror: %d\n", err);
                        exit(1);
                }

                SDL_GL_SwapWindow(window);
        }

        glDeleteProgram(program);
        glDeleteVertexArrays(1, &vao_terrain);
        glDeleteVertexArrays(1, &vao_model);

        free_model(&model);
        free_model(&terrain);

        return 0;
}
