#include <stdlib.h>
#include <string.h>
#include <GL/glew.h>
#include <math.h>

#include "model.h"

void create_model(struct model *m)
{
        GLfloat verts[] = {
                /* vertices     texture */
                /* front */
                -1, -1, -1,     1, 1,
                 1, -1, -1,     0, 1,
                 1,  1, -1,     0, 0,
                -1,  1, -1,     1, 0,
                /* back */
                -1, -1,  1,     1, 1,
                 1, -1,  1,     0, 1,
                 1,  1,  1,     0, 0,
                -1,  1,  1,     1, 0,
                /* top */
                -1,  1, -1,     1, 1,
                 1,  1, -1,     0, 1,
                 1,  1,  1,     0, 0,
                -1,  1,  1,     1, 0,
                /* bottom */
                -1, -1, -1,     1, 1,
                 1, -1, -1,     0, 1,
                 1, -1,  1,     0, 0,
                -1, -1,  1,     1, 0,
                /* left */
                -1, -1,  1,     1, 1,
                -1, -1, -1,     0, 1,
                -1,  1, -1,     0, 0,
                -1,  1,  1,     1, 0,
                /* right */
                 1, -1, -1,     1, 1,
                 1, -1,  1,     0, 1,
                 1,  1,  1,     0, 0,
                 1,  1, -1,     1, 0,
        };

        GLushort index[] = {
                /* front */
                 0,  1,  2,
                 2,  3,  0,
                /* top */
                 8,  9, 10,
                10, 11,  8,
                /* back */
                 4,  5,  6,
                 6,  7,  4,
                /* bottom */
                12, 13, 14,
                14, 15, 12,
                /* left */
                16, 17, 18,
                18, 19, 16,
                /* right */
                20, 21, 22,
                22, 23, 20
        };


        m->vsize = sizeof verts;
        if (!(m->verts = malloc(m->vsize)))
                abort();
        memcpy(m->verts, verts, m->vsize);

        m->isize = sizeof index;
        if (!(m->idx = malloc(m->isize)))
                abort();
        memcpy(m->idx, index, m->isize);
}

void create_terrain(struct model *m)
{
        const int width = 100;
        const int height = 100;
        int i, x, y;
        int tlen, ilen;

        GLfloat tile[] = {
             /* vertices     texture */
                0, -1, 0,     1, 1,
                1, -1, 0,     0, 1,
                1, -1, 1,     0, 0,
                0, -1, 1,     1, 0,
        };
        tlen = sizeof tile / sizeof tile[0];

        GLushort index[] = {
                0,  1,  2,
                2,  3,  0,
        };
        ilen = sizeof index / sizeof index[0];


        m->vsize = (sizeof tile) * width * height;
        if (!(m->verts = malloc(m->vsize)))
                abort();

        m->isize = (sizeof index) * width * height;
        if (!(m->idx = malloc(m->isize)))
                abort();

        for (x = 0; x<width; x++) {
                for (y = 0; y<height; y++) {
                        size_t toffset = (tlen * height * x) + tlen * y;
                        size_t ioffset = (ilen * height * x) + ilen * y;

                        memcpy(&m->verts[toffset], tile, sizeof tile);

                        for (i = 0; i<20; i+=5) {
                                m->verts[toffset+i] += x;
                                m->verts[toffset+i+2] += y;
                        }

                        memcpy(&m->idx[ioffset], index, sizeof index);
                }
        }

        for (x = 0, i = 0; x < (m->isize / sizeof m->idx[0]); x+=6, i+=4)
                for (y = 0; y<6; y++)
                        m->idx[x+y] += i;

        for (x = 0; x < (m->vsize / sizeof m->verts[0]); x+= sizeof tile / sizeof m->verts[0]) {
                m->verts[x+1] = sin(sqrt((m->verts[x+2] * m->verts[x+2])
                                        + (m->verts[x] * m->verts[x])));
                m->verts[x+6] = sin(sqrt((m->verts[x+7] * m->verts[x+7])
                                        + (m->verts[x+5] * m->verts[x+5])));
                m->verts[x+11] = sin(sqrt((m->verts[x+12] * m->verts[x+12])
                                        + (m->verts[x+10] * m->verts[x+10])));
                m->verts[x+16] = sin(sqrt((m->verts[x+17] * m->verts[x+17])
                                        + (m->verts[x+15] * m->verts[x+15])));
        }

}

void free_model(struct model *m)
{
        free(m->verts);
        free(m->idx);
}
