CFLAGS=-g -Wall -Werror --std=c11 -pedantic
OBJS=glthing.o gl.o sdl.o transforms.o jpeg.o model.o game.o

glthing: $(OBJS) Makefile
	cc $(CFLAGS) $(OBJS) -o glthing -ljpeg -lm -lSDL2 -lGLEW -lGL

test-matrix: test-matrix.o
	cc $(CFLAGS) test-matrix.o -o test-matrix -lm

.PHONY: clean test run

clean:
	rm -f glthing *.o

test: test-matrix
	./test-matrix

run: glthing Makefile
	./glthing

# DO NOT DELETE

game.o: /usr/include/math.h /usr/include/features.h
game.o: /usr/include/stdc-predef.h game.h sdl.h /usr/include/SDL2/SDL.h
game.o: /usr/include/SDL2/SDL_main.h /usr/include/SDL2/SDL_stdinc.h
game.o: /usr/include/SDL2/SDL_config.h /usr/include/SDL2/SDL_platform.h
game.o: /usr/include/SDL2/begin_code.h /usr/include/SDL2/close_code.h
game.o: /usr/include/stdio.h /usr/include/libio.h /usr/include/_G_config.h
game.o: /usr/include/wchar.h /usr/include/stdlib.h /usr/include/alloca.h
game.o: /usr/include/string.h /usr/include/xlocale.h /usr/include/strings.h
game.o: /usr/include/inttypes.h /usr/include/stdint.h /usr/include/ctype.h
game.o: /usr/include/endian.h /usr/include/SDL2/SDL_assert.h
game.o: /usr/include/SDL2/SDL_atomic.h /usr/include/SDL2/SDL_audio.h
game.o: /usr/include/SDL2/SDL_error.h /usr/include/SDL2/SDL_endian.h
game.o: /usr/include/SDL2/SDL_mutex.h /usr/include/SDL2/SDL_thread.h
game.o: /usr/include/SDL2/SDL_rwops.h /usr/include/SDL2/SDL_clipboard.h
game.o: /usr/include/SDL2/SDL_cpuinfo.h /usr/include/SDL2/SDL_events.h
game.o: /usr/include/SDL2/SDL_video.h /usr/include/SDL2/SDL_pixels.h
game.o: /usr/include/SDL2/SDL_rect.h /usr/include/SDL2/SDL_surface.h
game.o: /usr/include/SDL2/SDL_blendmode.h /usr/include/SDL2/SDL_keyboard.h
game.o: /usr/include/SDL2/SDL_keycode.h /usr/include/SDL2/SDL_scancode.h
game.o: /usr/include/SDL2/SDL_mouse.h /usr/include/SDL2/SDL_joystick.h
game.o: /usr/include/SDL2/SDL_gamecontroller.h /usr/include/SDL2/SDL_quit.h
game.o: /usr/include/SDL2/SDL_gesture.h /usr/include/SDL2/SDL_touch.h
game.o: /usr/include/SDL2/SDL_filesystem.h /usr/include/SDL2/SDL_haptic.h
game.o: /usr/include/SDL2/SDL_hints.h /usr/include/SDL2/SDL_loadso.h
game.o: /usr/include/SDL2/SDL_log.h /usr/include/SDL2/SDL_messagebox.h
game.o: /usr/include/SDL2/SDL_power.h /usr/include/SDL2/SDL_render.h
game.o: /usr/include/SDL2/SDL_system.h /usr/include/SDL2/SDL_timer.h
game.o: /usr/include/SDL2/SDL_version.h transforms.h
gl.o: gl.h /usr/include/GL/glew.h /usr/include/stdint.h
gl.o: /usr/include/features.h /usr/include/stdc-predef.h
gl.o: /usr/include/GL/glu.h /usr/include/GL/gl.h model.h sdl.h
gl.o: /usr/include/SDL2/SDL.h /usr/include/SDL2/SDL_main.h
gl.o: /usr/include/SDL2/SDL_stdinc.h /usr/include/SDL2/SDL_config.h
gl.o: /usr/include/SDL2/SDL_platform.h /usr/include/SDL2/begin_code.h
gl.o: /usr/include/SDL2/close_code.h /usr/include/stdio.h
gl.o: /usr/include/libio.h /usr/include/_G_config.h /usr/include/wchar.h
gl.o: /usr/include/stdlib.h /usr/include/alloca.h /usr/include/string.h
gl.o: /usr/include/xlocale.h /usr/include/strings.h /usr/include/inttypes.h
gl.o: /usr/include/ctype.h /usr/include/endian.h /usr/include/math.h
gl.o: /usr/include/SDL2/SDL_assert.h /usr/include/SDL2/SDL_atomic.h
gl.o: /usr/include/SDL2/SDL_audio.h /usr/include/SDL2/SDL_error.h
gl.o: /usr/include/SDL2/SDL_endian.h /usr/include/SDL2/SDL_mutex.h
gl.o: /usr/include/SDL2/SDL_thread.h /usr/include/SDL2/SDL_rwops.h
gl.o: /usr/include/SDL2/SDL_clipboard.h /usr/include/SDL2/SDL_cpuinfo.h
gl.o: /usr/include/SDL2/SDL_events.h /usr/include/SDL2/SDL_video.h
gl.o: /usr/include/SDL2/SDL_pixels.h /usr/include/SDL2/SDL_rect.h
gl.o: /usr/include/SDL2/SDL_surface.h /usr/include/SDL2/SDL_blendmode.h
gl.o: /usr/include/SDL2/SDL_keyboard.h /usr/include/SDL2/SDL_keycode.h
gl.o: /usr/include/SDL2/SDL_scancode.h /usr/include/SDL2/SDL_mouse.h
gl.o: /usr/include/SDL2/SDL_joystick.h /usr/include/SDL2/SDL_gamecontroller.h
gl.o: /usr/include/SDL2/SDL_quit.h /usr/include/SDL2/SDL_gesture.h
gl.o: /usr/include/SDL2/SDL_touch.h /usr/include/SDL2/SDL_filesystem.h
gl.o: /usr/include/SDL2/SDL_haptic.h /usr/include/SDL2/SDL_hints.h
gl.o: /usr/include/SDL2/SDL_loadso.h /usr/include/SDL2/SDL_log.h
gl.o: /usr/include/SDL2/SDL_messagebox.h /usr/include/SDL2/SDL_power.h
gl.o: /usr/include/SDL2/SDL_render.h /usr/include/SDL2/SDL_system.h
gl.o: /usr/include/SDL2/SDL_timer.h /usr/include/SDL2/SDL_version.h jpeg.h
gl.o: transforms.h
glthing.o: /usr/include/time.h /usr/include/features.h
glthing.o: /usr/include/stdc-predef.h /usr/include/xlocale.h game.h sdl.h
glthing.o: /usr/include/SDL2/SDL.h /usr/include/SDL2/SDL_main.h
glthing.o: /usr/include/SDL2/SDL_stdinc.h /usr/include/SDL2/SDL_config.h
glthing.o: /usr/include/SDL2/SDL_platform.h /usr/include/SDL2/begin_code.h
glthing.o: /usr/include/SDL2/close_code.h /usr/include/stdio.h
glthing.o: /usr/include/libio.h /usr/include/_G_config.h /usr/include/wchar.h
glthing.o: /usr/include/stdlib.h /usr/include/alloca.h /usr/include/string.h
glthing.o: /usr/include/strings.h /usr/include/inttypes.h
glthing.o: /usr/include/stdint.h /usr/include/ctype.h /usr/include/endian.h
glthing.o: /usr/include/math.h /usr/include/SDL2/SDL_assert.h
glthing.o: /usr/include/SDL2/SDL_atomic.h /usr/include/SDL2/SDL_audio.h
glthing.o: /usr/include/SDL2/SDL_error.h /usr/include/SDL2/SDL_endian.h
glthing.o: /usr/include/SDL2/SDL_mutex.h /usr/include/SDL2/SDL_thread.h
glthing.o: /usr/include/SDL2/SDL_rwops.h /usr/include/SDL2/SDL_clipboard.h
glthing.o: /usr/include/SDL2/SDL_cpuinfo.h /usr/include/SDL2/SDL_events.h
glthing.o: /usr/include/SDL2/SDL_video.h /usr/include/SDL2/SDL_pixels.h
glthing.o: /usr/include/SDL2/SDL_rect.h /usr/include/SDL2/SDL_surface.h
glthing.o: /usr/include/SDL2/SDL_blendmode.h /usr/include/SDL2/SDL_keyboard.h
glthing.o: /usr/include/SDL2/SDL_keycode.h /usr/include/SDL2/SDL_scancode.h
glthing.o: /usr/include/SDL2/SDL_mouse.h /usr/include/SDL2/SDL_joystick.h
glthing.o: /usr/include/SDL2/SDL_gamecontroller.h
glthing.o: /usr/include/SDL2/SDL_quit.h /usr/include/SDL2/SDL_gesture.h
glthing.o: /usr/include/SDL2/SDL_touch.h /usr/include/SDL2/SDL_filesystem.h
glthing.o: /usr/include/SDL2/SDL_haptic.h /usr/include/SDL2/SDL_hints.h
glthing.o: /usr/include/SDL2/SDL_loadso.h /usr/include/SDL2/SDL_log.h
glthing.o: /usr/include/SDL2/SDL_messagebox.h /usr/include/SDL2/SDL_power.h
glthing.o: /usr/include/SDL2/SDL_render.h /usr/include/SDL2/SDL_system.h
glthing.o: /usr/include/SDL2/SDL_timer.h /usr/include/SDL2/SDL_version.h
glthing.o: transforms.h gl.h /usr/include/GL/glew.h /usr/include/GL/glu.h
glthing.o: /usr/include/GL/gl.h model.h
jpeg.o: /usr/include/stdio.h /usr/include/features.h
jpeg.o: /usr/include/stdc-predef.h /usr/include/libio.h
jpeg.o: /usr/include/_G_config.h /usr/include/wchar.h /usr/include/stdlib.h
jpeg.o: /usr/include/alloca.h /usr/include/jpeglib.h /usr/include/jmorecfg.h
jpeg.o: jpeg.h
model.o: /usr/include/stdlib.h /usr/include/features.h
model.o: /usr/include/stdc-predef.h /usr/include/alloca.h
model.o: /usr/include/string.h /usr/include/xlocale.h /usr/include/GL/glew.h
model.o: /usr/include/stdint.h /usr/include/GL/glu.h /usr/include/GL/gl.h
model.o: /usr/include/math.h model.h
sdl.o: sdl.h /usr/include/SDL2/SDL.h /usr/include/SDL2/SDL_main.h
sdl.o: /usr/include/SDL2/SDL_stdinc.h /usr/include/SDL2/SDL_config.h
sdl.o: /usr/include/SDL2/SDL_platform.h /usr/include/SDL2/begin_code.h
sdl.o: /usr/include/SDL2/close_code.h /usr/include/stdio.h
sdl.o: /usr/include/features.h /usr/include/stdc-predef.h
sdl.o: /usr/include/libio.h /usr/include/_G_config.h /usr/include/wchar.h
sdl.o: /usr/include/stdlib.h /usr/include/alloca.h /usr/include/string.h
sdl.o: /usr/include/xlocale.h /usr/include/strings.h /usr/include/inttypes.h
sdl.o: /usr/include/stdint.h /usr/include/ctype.h /usr/include/endian.h
sdl.o: /usr/include/math.h /usr/include/SDL2/SDL_assert.h
sdl.o: /usr/include/SDL2/SDL_atomic.h /usr/include/SDL2/SDL_audio.h
sdl.o: /usr/include/SDL2/SDL_error.h /usr/include/SDL2/SDL_endian.h
sdl.o: /usr/include/SDL2/SDL_mutex.h /usr/include/SDL2/SDL_thread.h
sdl.o: /usr/include/SDL2/SDL_rwops.h /usr/include/SDL2/SDL_clipboard.h
sdl.o: /usr/include/SDL2/SDL_cpuinfo.h /usr/include/SDL2/SDL_events.h
sdl.o: /usr/include/SDL2/SDL_video.h /usr/include/SDL2/SDL_pixels.h
sdl.o: /usr/include/SDL2/SDL_rect.h /usr/include/SDL2/SDL_surface.h
sdl.o: /usr/include/SDL2/SDL_blendmode.h /usr/include/SDL2/SDL_keyboard.h
sdl.o: /usr/include/SDL2/SDL_keycode.h /usr/include/SDL2/SDL_scancode.h
sdl.o: /usr/include/SDL2/SDL_mouse.h /usr/include/SDL2/SDL_joystick.h
sdl.o: /usr/include/SDL2/SDL_gamecontroller.h /usr/include/SDL2/SDL_quit.h
sdl.o: /usr/include/SDL2/SDL_gesture.h /usr/include/SDL2/SDL_touch.h
sdl.o: /usr/include/SDL2/SDL_filesystem.h /usr/include/SDL2/SDL_haptic.h
sdl.o: /usr/include/SDL2/SDL_hints.h /usr/include/SDL2/SDL_loadso.h
sdl.o: /usr/include/SDL2/SDL_log.h /usr/include/SDL2/SDL_messagebox.h
sdl.o: /usr/include/SDL2/SDL_power.h /usr/include/SDL2/SDL_render.h
sdl.o: /usr/include/SDL2/SDL_system.h /usr/include/SDL2/SDL_timer.h
sdl.o: /usr/include/SDL2/SDL_version.h
test-matrix.o: /usr/include/assert.h /usr/include/features.h
test-matrix.o: /usr/include/stdc-predef.h /usr/include/stdio.h
test-matrix.o: /usr/include/libio.h /usr/include/_G_config.h
test-matrix.o: /usr/include/wchar.h transforms.c /usr/include/math.h
test-matrix.o: transforms.h
transforms.o: /usr/include/assert.h /usr/include/features.h
transforms.o: /usr/include/stdc-predef.h /usr/include/math.h
transforms.o: /usr/include/stdio.h /usr/include/libio.h
transforms.o: /usr/include/_G_config.h /usr/include/wchar.h transforms.h
