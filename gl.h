#ifndef GL_H
#define GL_H

#include <GL/glew.h>

#include "model.h"
#include "sdl.h"

void setup_gl(struct resolution *res);

void set_perspective(GLuint *program, struct resolution *res);

void load_tex(char *path);

GLuint vao_setup(struct model *m);

GLuint compile(void);

#endif
