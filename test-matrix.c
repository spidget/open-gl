#include <assert.h>
#include <stdio.h>

#include "transforms.c"

void one(void)
{
        printf("one\n");

        float one[16] = { 1,  2,  3,  4,
                          5,  6,  7,  8,
                          9, 10, 11, 12,
                          13, 14, 15, 16};

        float two[16] = {17, 18, 19, 20,
                         21, 22, 23, 24,
                         25, 26, 27, 28,
                         29, 30, 31, 32};

        mul(one, two);

        assert(one[0]  ==  538);
        assert(one[1]  ==  612);
        assert(one[2]  ==  686);
        assert(one[3]  ==  760);
        assert(one[4]  ==  650);
        assert(one[5]  ==  740);
        assert(one[6]  ==  830);
        assert(one[7]  ==  920);
        assert(one[8]  ==  762);
        assert(one[9]  ==  868);
        assert(one[10] ==  974);
        assert(one[11] == 1080);
        assert(one[12] ==  874);
        assert(one[13] ==  996);
        assert(one[14] == 1118);
        assert(one[15] == 1240);
}

void two(void)
{
        printf("two\n");

        float one[16] = { 1,  2,  3,  4,
                          5,  6,  7,  8,
                          9, 10, 11, 12,
                          13, 14, 15, 16};

        move(one, 0, 0, 0);

        assert(one[0]  ==  1);
        assert(one[1]  ==  2);
        assert(one[2]  ==  3);
        assert(one[3]  ==  4);
        assert(one[4]  ==  5);
        assert(one[5]  ==  6);
        assert(one[6]  ==  7);
        assert(one[7]  ==  8);
        assert(one[8]  ==  9);
        assert(one[9]  == 10);
        assert(one[10] == 11);
        assert(one[11] == 12);
        assert(one[12] == 13);
        assert(one[13] == 14);
        assert(one[14] == 15);
        assert(one[15] == 16);
}

void three(void)
{
        printf("three\n");

        float one[16];

        identity(one);
        move(one, 1, 1, -5);

        assert(one[0]  ==  1);
        assert(one[1]  ==  0);
        assert(one[2]  ==  0);
        assert(one[3]  ==  0);
        assert(one[4]  ==  0);
        assert(one[5]  ==  1);
        assert(one[6]  ==  0);
        assert(one[7]  ==  0);
        assert(one[8]  ==  0);
        assert(one[9]  ==  0);
        assert(one[10] ==  1);
        assert(one[11] ==  0);
        assert(one[12] ==  1);
        assert(one[13] ==  1);
        assert(one[14] == -5);
        assert(one[15] ==  1);
}

int main(int argc, char **argv)
{
        one();
        two();
        three();
}
