#ifndef JPEG_H
#define JPEG_H

struct jpeg {
        unsigned char *b;
        int height;
        int width;
};
void read_jpeg(struct jpeg *jpeg, char *path);

#endif
