#ifndef SDL_H
#define SDL_H

#include <SDL2/SDL.h>

SDL_Window *init_sdl(void);

struct input {
        struct keys {
                unsigned int forward: 1;
                unsigned int backward: 1;
                unsigned int left: 1;
                unsigned int right: 1;
                unsigned int jump: 1;
                unsigned int crouch: 1;
                unsigned int spin: 1;
                unsigned int fullscreen: 1;
                unsigned int resize: 1;
        } keys;
        int x;
        int y;
};

struct resolution {
        int width;
        int height;
};
void get_resolution(struct resolution *r);

int sdl_input(struct input *input, struct resolution *res);

#endif
