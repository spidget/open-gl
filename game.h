#ifndef GAME_H
#define GAME_H

#include "sdl.h"
#include "transforms.h"

void move_cam(struct input *input, struct camera *cam);

#endif
