#include <math.h>

#include "game.h"

void move_cam(struct input *input, struct camera *cam)
{
        static float x = 0;
        static float y = 0;

        if (input->keys.forward)
                move_vec(&cam->pos, &cam->dir, 0.1);

        if (input->keys.backward)
                move_vec(&cam->pos, &cam->dir, -0.1);

        if (input->keys.left) {
                struct vec side;

                cross_vec(&side, &cam->up, &cam->dir);
                move_vec(&cam->pos, &side, 0.1);
        }

        if (input->keys.right) {
                struct vec side;

                cross_vec(&side, &cam->up, &cam->dir);
                move_vec(&cam->pos, &side, -0.1);
        }

        if (input->keys.jump)
                move_vec(&cam->pos, &cam->up, 0.1);

        if (input->keys.crouch)
                move_vec(&cam->pos, &cam->up, -0.1);

        x += -input->x * 0.007;
        y += -input->y * 0.007;

        if (y > 1.3)
                y = 1.3;
        else if (y < -1.3)
                y = -1.3;

        cam->dir.x = cos(y) * sin(x);
        cam->dir.y = sin(y);
        cam->dir.z = cos(y) * cos(x);
}
