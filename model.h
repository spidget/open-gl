#ifndef MODEL_H
#define MODEL_H

struct model {
        GLfloat *verts;
        size_t vsize;
        GLushort *idx;
        size_t isize;
};

void create_model(struct model *m);
void create_terrain(struct model *m);

void free_model(struct model *m);

#endif
