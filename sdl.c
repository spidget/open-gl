#include "sdl.h"

static void sdl_fail()
{
        fprintf(stderr, "\n%s\n", SDL_GetError());
        exit(1);
}

SDL_Window *init_sdl()
{
        SDL_Window *window;

        if (SDL_Init(SDL_INIT_VIDEO) != 0)
                sdl_fail();
        atexit(SDL_Quit);

        window = SDL_CreateWindow("SDL",
                                  SDL_WINDOWPOS_CENTERED,
                                  SDL_WINDOWPOS_CENTERED,
                                  640,
                                  480,
                                  SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_WINDOW_OPENGL);
        if (!window)
                sdl_fail();

        if (SDL_ShowCursor(SDL_DISABLE) < 0)
                sdl_fail();
        if (SDL_SetRelativeMouseMode(SDL_TRUE) < 0)
                sdl_fail();

        /* initialise opengl */
        if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3) != 0)
                sdl_fail();
        if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3) != 0)
                sdl_fail();
        if (SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1) != 0)
                sdl_fail();

        if (!SDL_GL_CreateContext(window))
                sdl_fail();

        SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1");

        return window;
}

int sdl_input(struct input *input, struct resolution *res)
{
        SDL_Event event;

        input->x = 0;
        input->y = 0;
        input->keys.fullscreen = 0;

        while (SDL_PollEvent(&event)) {
                switch (event.type) {
                case SDL_QUIT:
                        return 0;
                case SDL_KEYDOWN:
                        switch (event.key.keysym.sym) {
                        case 'w':
                                input->keys.forward = 1;
                                continue;
                        case 's':
                                input->keys.backward = 1;
                                continue;
                        case 'a':
                                input->keys.left = 1;
                                continue;
                        case 'd':
                                input->keys.right = 1;
                                continue;
                        case ' ':
                                input->keys.jump = 1;
                                continue;
                        case 'z':
                                input->keys.crouch = 1;
                                continue;
                        case '#':
                                input->keys.spin = ~input->keys.spin;
                                continue;
                        case SDLK_ESCAPE:
                                return 0;
                        }
                        continue;
                case SDL_KEYUP:
                        switch (event.key.keysym.sym) {
                        case 'w':
                                input->keys.forward = 0;
                                continue;
                        case 's':
                                input->keys.backward = 0;
                                continue;
                        case 'a':
                                input->keys.left = 0;
                                continue;
                        case 'd':
                                input->keys.right = 0;
                                continue;
                        case ' ':
                                input->keys.jump = 0;
                                continue;
                        case 'z':
                                input->keys.crouch = 0;
                                continue;
                        case SDLK_F11:
                                input->keys.fullscreen = 1;
                                continue;
                        }
                        continue;
                case SDL_MOUSEMOTION:
                        input->x = event.motion.xrel;
                        input->y = event.motion.yrel;
                        continue;
                case SDL_WINDOWEVENT:
                        if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
                                input->keys.resize = 1;
                                res->width = event.window.data1;
                                res->height = event.window.data2;
                        }
                        continue;
                }
        }

        return 1;
}

void get_resolution(struct resolution *r)
{
        SDL_DisplayMode mode;

        if (SDL_GetCurrentDisplayMode(0, &mode) != 0)
                sdl_fail();

        r->width = mode.w;
        r->height = mode.h;
}
